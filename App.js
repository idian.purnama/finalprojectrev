
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Absensi from './src'
import Report from './src/Pages/Report'
import Abs from './src/Pages/Absensi'

export default function App() {
  return (
    <Absensi/>
  ); 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
