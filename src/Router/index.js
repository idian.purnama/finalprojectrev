import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import Icon from 'react-native-vector-icons/FontAwesome';
import LoginScreen from '../Pages/Login'
import AboutScreen from '../Pages/AboutScreen'
import Absensi from '../Pages/Absensi'
import Report from '../Pages/Report'
import AsyncStorage from '@react-native-async-storage/async-storage';


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
export default function Router() {  
const [username, setUsername] = useState('');

console.log("-----------"+username);
useEffect(() => {
  AsyncStorage.getItem('username').then(
    (value) =>
      setUsername(value)
  );
}, []);
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="screen" screenOptions={{headerShown:false}}>
              <Stack.Screen headerMode="screen" name="LoginScreen" component={LoginScreen}/> 
              <Stack.Screen headerMode="screen" name="MainApp" component={MainApp}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
const MainApp =()=>( 

        <Tab.Navigator screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'ABSENSI') {
                iconName = focused
                  ? 'camera-retro'
                  : 'camera-retro';
              }else if (route.name === 'REPORT') {
                iconName = focused
                  ? 'calendar'
                  : 'calendar';
              }else if (route.name === 'ACCOUNT') {
                iconName = focused
                  ? 'user-md'
                  : 'user-md';
              }
  
              return <Icon name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'gray',
            headerShown:false
          })}>
            <Tab.Screen name="REPORT" component={Report}/>
            <Tab.Screen name="ABSENSI" component={Absensi}/>
            <Tab.Screen name="ACCOUNT" component={AboutScreen}/>
        </Tab.Navigator>
)
