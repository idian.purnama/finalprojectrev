import axios from 'axios';
import React, {useEffect, useState} from 'react'
import { Button, TouchableHighlight, StyleSheet, Text, View, Image, Alert } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import MapView, { Marker, Circle } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {getDistance, getPreciseDistance} from 'geolib';

export default function Absensi() {
    const [username, setUsername] = useState('');

    const [jam, setjam] = useState('');
    const [tanggal, settanggal] = useState('');
    const [check_in, setcheck_in] = useState('');
    const [check_in_lokasi, setcheck_in_lokasi] = useState('');
    const [check_out, setcheck_out] = useState('');
    const [check_out_lokasi, setcheck_out_lokasi] = useState('');
    const [position, setPosition] = useState({
        latitude: 10,
        longitude: 10,
        latitudeDelta: 0.001,
        longitudeDelta: 0.001,
    });
    const [lokasi, setLokasi] = useState({
        latitude: -7.850181087835965,
        longitude: 112.70815669231281,
        radius: 25,
        id_absen: 1,
    })
    const GetLocation =()=>{
        Geolocation.getCurrentPosition((pos) => {
            const crd = pos.coords;
            setPosition({
              latitude: crd.latitude,
              longitude: crd.longitude,
              latitudeDelta: 0.001,
              longitudeDelta: 0.001,
            });
          }).catch((err) => {
            console.log(err);
          });
    }
  
    const GetData =()=>{
        AsyncStorage.getItem('username').then(
            (value) =>{
                setUsername(value)
                var bodyFormData = new FormData();
                bodyFormData.append('username', value);

                axios({
                    method: "POST",
                    url: "http://202.162.221.213:8081/tesApi.php?status=absensi", 
                    data: bodyFormData,
                    headers: { "Content-Type": "multipart/form-data" }, 
                })
                .then(function (response) {
                    const res = response.data;
                    console.log(res);
                    if(res.status=='200' && res.success==true){
                        setjam(res.jam)
                        settanggal(res.tanggal)
                        setcheck_in(res.check_in)
                        setcheck_in_lokasi(res.check_in_lokasi)
                        setcheck_out(res.check_out)
                        setcheck_out_lokasi(res.check_out_lokasi)
                        
                    } 
                })
                .catch(function (response) {
                    console.log(response);
                });      
            }
              
        ); 
    }

    const absenDatang = () => {
        var dis = getDistance(
            {latitude: position.latitude, longitude: position.longitude},
            {latitude: lokasi.latitude, longitude: lokasi.longitude},
        );
        if(lokasi.radius>=dis){
            var bodyFormData = new FormData();
            bodyFormData.append('username', username);
            bodyFormData.append('latitude', position.latitude);
            bodyFormData.append('longitude', position.longitude);
            bodyFormData.append('id_absen', lokasi.id_absen);
            axios({
                method: "POST",
                url: "http://202.162.221.213:8081/tesApi.php?status=absenDatang",
                data: bodyFormData,
                headers: { "Content-Type": "multipart/form-data" },
            })
            .then(function (response) {
                const res = response.data;
                console.log(res);
                if(res.status=='200' && res.success==true){
                    setcheck_in(res.data.check_in)
                    setcheck_in_lokasi(res.data.check_in_lokasi)
                    Alert.alert('Warning', 'Sukses Menyimpan',[
                        {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
                    ]);
                }else{
                    Alert.alert('Error', 'Mohon ulangi Kembali',[
                        {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
                    ]);
                }
            })
            .catch(function (response) {
                console.log(response);
            }); 
        }else{
            Alert.alert('Error', 'Anda tidak dalam radius absensi',[
                {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
            ]);
        }
    };

    const absenPulang = () => {
        var dis = getDistance(
            {latitude: position.latitude, longitude: position.longitude},
            {latitude: lokasi.latitude, longitude: lokasi.longitude},
        );
        if(lokasi.radius>=dis){
            var bodyFormData = new FormData();
            bodyFormData.append('username', username);
            bodyFormData.append('latitude', position.latitude);
            bodyFormData.append('longitude', position.longitude);
            bodyFormData.append('id_absen', lokasi.id_absen);
            axios({
                method: "POST",
                url: "http://202.162.221.213:8081/tesApi.php?status=absenPulang",
                data: bodyFormData,
                headers: { "Content-Type": "multipart/form-data" },
            })
            .then(function (response) {
                const res = response.data;
                if(res.status=='200' && res.success==true){
                    setcheck_out(res.data.check_out)
                    setcheck_out_lokasi(res.data.check_out_lokasi)
                    Alert.alert('Warning', 'Sukses Menyimpan',[
                        {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
                    ]);
                }else{
                    Alert.alert('Error', 'Mohon ulangi Kembali',[
                        {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
                    ]);
                }
            })
            .catch(function (response) {
                console.log(response);
            }); 
        }else{
            Alert.alert('Error', 'Anda tidak dalam radius absensi',[
                {text:'Tutup', onPress:()=> console.log('TUtup Alert')}
            ]);
        }
    };

    useEffect(()=>{
        GetData()
        GetLocation()

    },[])
    return (
        <View style={styles.container}>
            <View style={{width:'100%', backgroundColor:'#ECECEC', padding:10}}>
                <View style={{width:'100%', paddingBottom:10, backgroundColor:'#FFF', borderRadius:10}}>
                    <View style={{justifyContent:'center', alignItems:'center', width:'100%', height:47, backgroundColor:'#1BB273', borderTopLeftRadius:10, borderTopRightRadius:10}}>
                        <Text style={{fontSize:16, color:'#FFF'}}>LAPORAN KEHADIRAN</Text>
                    </View>
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{fontSize:35, color:'#00190E'}}>{jam}</Text>
                        <Text style={{fontSize:14, color:'#00190E'}}>{tanggal}</Text>
                    </View>
                </View>                
            </View>
            <View style={{padding:10, width:'100%'}}>
                <View style={{width:'100%', height:400, backgroundColor:'#EDEDED'}}>
                    <MapView style={styles.map} loadingEnabled={true} initialRegion={position} showsUserLocation={true}>
                    
                    <Circle
                        center={{
                            latitude: lokasi.latitude,
                            longitude: lokasi.longitude,
                        }}
                        radius={lokasi.radius}
                        fillColor='rgba(253, 48, 4,0.5)'
                        strokeColor='rgba(253, 48, 4,1)'
                    />
                    </MapView>
                </View>
                <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
                    <TouchableHighlight style={{width:55, height:55, borderRadius:28, justifyContent:'center', alignItems:'center'}} onPress={absenDatang} underlayColor='#057144'>
                        <View style={{width:55, height:55, backgroundColor:'#1BB273', justifyContent:'center', alignItems:'center', borderRadius:28}}>
                            <Image
                                style={{ height: 28, width: 28 }}
                                source={require('../assets/in.png')}
                            />
                        </View>
                    </TouchableHighlight>
                    <View style={{width:'80%', height:55}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <View style={{width:'100%', paddingLeft:10, height:55, justifyContent:'center'}}>
                                <Text style={{fontSize:20, color:'#656565'}}>{check_in}</Text>
                                <Text style={{fontSize:12, fontWeight:'100', color:'#00190E'}}>{check_in_lokasi}</Text>
                            </View>
                        </View>
                    </View> 
                </View>
                <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
                    <TouchableHighlight style={{width:55, height:55, borderRadius:28, justifyContent:'center', alignItems:'center'}} onPress={absenPulang} underlayColor='#057144'>
                        <View style={{width:55, height:55, backgroundColor:'#FF6600', justifyContent:'center', alignItems:'center', borderRadius:28}}>
                            <Image
                                style={{ height: 28, width:28 }}
                                source={require('../assets/out.png')}
                            />
                        </View>
                    </TouchableHighlight>
                    <View style={{width:'80%', height:55}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <View style={{width:'100%', paddingLeft:10, height:55, justifyContent:'center'}}>
                                <Text style={{fontSize:20, color:'#656565'}}>{check_out}</Text>
                                <Text style={{fontSize:12, fontWeight:'100', color:'#00190E'}}>{check_out_lokasi}</Text>
                            </View>
                        </View>
                    </View> 
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#fff'
    },
    map: {
      width: '100%',
      height: '100%',
    },
})
