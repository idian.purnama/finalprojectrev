import axios from 'axios';
import React, {useEffect, useState} from 'react'
import { SafeAreaView, FlatList, Image, StyleSheet, Text, View, TouchableHighlight, ScrollView} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function AboutScreen({navigation}) {
    const [nama, setNama] = useState('');
    const [username, setUsername] = useState('');
    const [bagian, setBagian] = useState('');
    const [posisi, setPosisi] = useState('');
    const [items, setItems] = useState('');
    AsyncStorage.getItem('nama').then(
        (value) =>
        setNama(value)
    );
    AsyncStorage.getItem('bagian').then(
        (value) =>
        setBagian(value)
    );
    AsyncStorage.getItem('posisi').then(
        (value) =>
        setPosisi(value)
    );
    AsyncStorage.getItem('username').then(
        (value) =>
        setUsername(value)
    );
    const logout = () => {
        return navigation.navigate("LoginScreen",{
            isLogin : false
        }) 
    };

    const GetData =()=>{
        AsyncStorage.getItem('username').then(
            (value) =>{
                var bodyFormData = new FormData();
                bodyFormData.append('username', value);
                axios({
                    method: "POST",
                    url: "http://202.162.221.213:8081/tesApi.php?status=rekap",
                    data: bodyFormData,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                .then(function (response) {
                    const res = response.data;
                    if(res.status=='200' && res.success==true){
                        const data1=(res.data)
                        console.log('Data Dari Server : ', data1)
                        setItems(data1)
                    }else{
                        
                    }
                })
                .catch(function (response) {
                    console.log(response);
                });
            }
        ); 
    }
    useEffect(()=>{
        GetData()
    },[])

    return (
        <View style={styles.container}>
            <View style={{width:'100%'}}>
                <View style={{backgroundColor:'#1BB273', width:'100%', alignItems:'center', justifyContent:'center', padding:10}}>
                    <View style={{width:120, height:120, borderRadius:60, backgroundColor:'#057144'}}></View>
                    <Text style={{fontSize:30, color:'#FFF'}}>{nama}</Text>
                    <Text style={{fontSize:19, color:'#FFF', fontWeight:'100'}}>{posisi}</Text>
                    <Text style={{fontSize:19, color:'#FFF', fontWeight:'100'}}>{bagian}</Text>
                </View>
                <View style={{padding:10, width:'100%'}}>
                    <View style={{backgroundColor:'#FFF', width:'100%'}}>
                        <ScrollView style={{width:'100%'}}>
                        <SafeAreaView style={{flex: 1}}>
                        <FlatList
                            data={items}
                            keyExtractor={(item, index)=>`${item.id}-${index}`}
                            renderItem={({item})=>{
                                return (
                                    <View style={{padding:10, borderBottomColor:'#ECECEC', borderBottomWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{color:'#4D4F5C'}}>{item.keterangan}</Text>
                                        <View style={{color:'#4D4F5C', backgroundColor:'#FF6600', width:30, borderRadius:3, alignItems:'center', height:22}}>
                                            <Text style={{color:'#FFF', fontWeight:'bold'}}>{item.jumlah}</Text>
                                        </View>
                                    </View>
                                )
                            }}
                            />
                        </SafeAreaView>
                        </ScrollView>
                    </View>
                </View>
                
                
            </View>
            
            
            <View style={{position:'absolute', bottom: 10, right: 10, width:50, height:50, borderRadius:25, backgroundColor:'#1BB273'}}>
            <TouchableHighlight style={{width:50, height:50, borderRadius:25, justifyContent:'center', alignItems:'center'}} onPress={logout} underlayColor='#057144'>
                <Image style={{width:32, height:32}} source={require('../assets/logout.png')} />
            </TouchableHighlight>

            </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
    }
})
