import axios from 'axios';
import React, {useEffect, useState } from 'react'
import { Alert, StyleSheet, Text, View, TextInput, TouchableHighlight, Image } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Login({navigation}) {
    const [username, setUsername] = useState('092100001');
    const [password, setPassword] = useState('12345');    
    const submit = () => {
        var bodyFormData = new FormData();
        bodyFormData.append('username', username);
        bodyFormData.append('password', password);
        axios({
            method: "post",
            url: "http://202.162.221.213:8081/tesApi.php?status=login",
            data: bodyFormData,
            headers: { "Content-Type": "multipart/form-data" },
        })
        .then(function (response) {
            const res = response.data;
            console.log(res);
            if(res.status=='200' && res.success==true){
                AsyncStorage.setItem('username', res.data.username);
                AsyncStorage.setItem('nama', res.data.nama);
                AsyncStorage.setItem('posisi', res.data.posisi);
                AsyncStorage.setItem('bagian', res.data.bagian);
                return navigation.navigate("MainApp",{
                    isLogin : true
                }) 
            }else{
                Alert.alert('ERROR', 'PASSWORD TIDAK DITEMUKAN',[
                    {text:'TUTUP', onPress:()=> console.log('TUtup Alert')}
                ]);
            }
        })
        .catch(function (response) {
                //handle error
            console.log(response);
        });      
    };

    return (
        <View style={styles.container}>
            <View style={{width:'100%', alignItems:'center', height:'100%', backgroundColor:'#1bb273', position:'absolute',}}>
                <Image
                    style={{ height: 120, width: 120, marginTop: 40 }}
                    source={require('../assets/logo.png')}
                />
                <View style={{width:'100%', alignItems:'center', height:'100%', backgroundColor:'#EBEBEB', 
                                position:'absolute', marginTop:200, padding:10,
                                borderTopLeftRadius:40, borderTopRightRadius:40,
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2, 
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                }}>
                    <Text style={{color:'#4D4F5C', fontSize:20, fontWeight:'500'}}>Silahkan Login e-Presensi</Text>
                </View>
                
                <View style={{width:'100%', alignItems:'center', height:'100%', backgroundColor:'#FFF', 
                                position:'absolute', marginTop:250, padding:40,
                                borderTopLeftRadius:40, borderTopRightRadius:40,
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,}}>
                    <Text style={{alignItems:'flex-start', width:'100%', color:'#1BB273'}}>Masukan NIP</Text>
                    <TextInput
                        style={styles.input}
                        keyboardType="numeric"
                        placeholder="Masukan 8 angka NIP anda"
                        value={username}
                        onChangeText={(value) => setUsername(value)}
                    />
                    <Text style={{alignItems:'flex-start', width:'100%', color:'#1BB273'}}>Masukan Sandi</Text>
                    <TextInput
                        style={styles.input}
                        secureTextEntry={true}
                        placeholder="Masukan sandi anda"
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />

                    
                    <TouchableHighlight
                    style={styles.submit}
                    onPress={submit}
                    underlayColor='#057144'>
                        <Text style={{color:'#FFF'}}>LOGIN</Text>
                    </TouchableHighlight>

                    <Text style={{color:'#DDD', fontSize:14, marginTop:50}}>#periode2021_2023</Text>
                </View>
            
                
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    submit: {
        justifyContent:'center',
        height:40,
        marginTop:40,
        backgroundColor: '#1BB273',
        borderRadius: 25,
        width:'100%',
        alignItems:'center',
        shadowColor: '#000',
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    submitText: {
        color: '#fff',
        fontWeight:'bold',
        textAlign: 'center',
    },
    input: {
        height: 40,
        width:'100%',
        borderWidth: 1,
        padding: 10,
        color:'#2c3e50',
        borderColor:'#1BB273',
        borderRadius:5,
        marginBottom:20,
      },
})
