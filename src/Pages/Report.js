import axios from 'axios';
import React, {useEffect, useState} from 'react'
import { SafeAreaView, StyleSheet, ScrollView, Text, View, FlatList } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Report({navigation}) {
    const [username, setUsername] = useState('');
    const [nama, setNama] = useState('');    
    const [posisi, setPosisi] = useState('');    
    const [bagian, setBagian] = useState('');   
    const [bulan, setBulan] = useState('');    

    const [items, setItems]=useState([]);

    
    
    AsyncStorage.getItem('username').then(
        (value) =>
          setUsername(value)
    );

    
    AsyncStorage.getItem('nama').then(
        (value) =>
          setNama(value)
    );

    
    AsyncStorage.getItem('posisi').then(
        (value) =>
          setPosisi(value)
    );

    
    AsyncStorage.getItem('bagian').then(
        (value) =>
        setBagian(value)
    );
    const GetData =()=>{
        AsyncStorage.getItem('username').then(
            (value) =>{
                setUsername(value)
                var bodyFormData = new FormData();
                bodyFormData.append('username', value);
                axios({
                    method: "POST",
                    url: "http://202.162.221.213:8081/tesApi.php?status=laporan",
                    data: bodyFormData,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                .then(function (response) {
                    const res = response.data;
                    if(res.status=='200' && res.success==true){
                        setBulan(res.bulan)
                        const data1=(res.data)
                        console.log('Data Dari Server : ', data1)
                        setItems(data1)
                    }else{
                        
                    }
                })
                .catch(function (response) {
                    console.log(response);
                });
            }
        ); 
    }
    useEffect(()=>{        
        GetData();
    },[GetData])
    return (
        <SafeAreaView style={styles.container}>
            <>
            <View style={{width:'100%', height:102, backgroundColor:'#FFF'}}>
                <View style={{justifyContent:'center', alignItems:'center', width:'100%', height:47, backgroundColor:'#1BB273'}}>
                    <Text style={{fontSize:16, color:'#FFF'}}>LAPORAN KEHADIRAN</Text>
                </View>
                <View style={{justifyContent:'center', marginTop:8, alignItems:'center'}}>
                    <Text style={{fontSize:14, color:'#00190E'}}>Laporan harian pada bulan {bulan}</Text>
                    <Text style={{fontSize:14, color:'#00190E'}}>{nama}</Text>
                </View>
            </View>
            </>
            
            <ScrollView style={{width:'100%', padding:10}}>

                <FlatList
                    data={items}
                    keyExtractor={(item, index)=>`${item.id}-${index}`}
                    renderItem={({item})=>{
                        return (
                            <View style={{width:'100%', backgroundColor:'#FFF', borderRadius:10, padding:10, marginBottom:10}}>
                                <View>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{width:45, height:45, backgroundColor:'#1BB273', justifyContent:'center', alignItems:'center', borderRadius:5}}>
                                            <Text style={{fontSize:20, fontWeight:'bold', color:'#FFF'}}>{item.tanggal}</Text>
                                            <Text style={{fontSize:10, fontWeight:'400', color:'#FFF', marginTop:-5}}>{item.day}</Text>
                                        </View>
                                        
                                        <View style={{width:'80%'}}>
                                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                                <View style={{width:'50%', paddingLeft:10}}>
                                                    <Text style={{fontSize:10, color:'#656565'}}>ABSEN MASUK</Text>
                                                    <Text style={{fontSize:13, fontWeight:'bold', color:'#00190E'}}>{item.check_in}</Text>
                                                    <Text style={[styles.status,item.s_datang=='1'?styles.status_1:((item.s_datang=='2')?styles.status_2:styles.status_3)]}>{item.status_datang}</Text>
                                                </View>
                                                <View style={{width:'50%', paddingLeft:10, alignItems:'flex-end'}}>
                                                    <Text style={{fontSize:10, color:'#656565'}}>ABSEN KELUAR</Text>
                                                    <Text style={{fontSize:13, fontWeight:'bold', color:'#00190E'}}>{item.check_out}</Text>
                                                    <Text style={[styles.status,item.s_pulang=='1'?styles.status_1:((item.s_pulang=='2')?styles.status_2:styles.status_3)]}>{item.status_pulang}</Text>
                                                </View>
                                            </View>
                                        </View> 
                                    </View>
                                </View>
                            </View>
                        )
                    }

                    }
                />
            </ScrollView>      
            </SafeAreaView>   
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        backgroundColor:'#ECECEC'
    },
    status:{
        fontSize:10
    },
    status_1:{
        fontSize:10, color:'red'
    },
    status_2:{
        fontSize:10, color:'#f39c12'
    },
    status_3:{
        fontSize:10, color:'#1300EE'
    }
})
